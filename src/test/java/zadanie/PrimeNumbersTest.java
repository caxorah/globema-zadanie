package zadanie;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PrimeNumbersTest {

    @Test
    void shouldReturnAllPrimeNumbersNotGreaterThan100() {
        List<Integer> primeNumbers = List.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97);
        List<Integer> result = PrimeNumbers.findPrimeNumbersNotGreaterThan(100);

        assertEquals(primeNumbers.size(), result.size());
        for (int i=0; i<primeNumbers.size(); i++) {
            assertEquals(primeNumbers.get(i), result.get(i));
        }
    }

    @Test
    void shouldReturnEmptyListIfArgumentIsLessThan3() {
        List<Integer> result = PrimeNumbers.findPrimeNumbersNotGreaterThan(2);
        assertTrue(result.isEmpty());
    }

}