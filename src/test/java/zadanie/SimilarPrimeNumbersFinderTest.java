package zadanie;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SimilarPrimeNumbersFinderTest {

    @Test
    void shouldReturnLargestSetOfCombinationFor1000ThatIsOfSize4() {
        SimilarPrimeNumbersFinder finder = new SimilarPrimeNumbersFinder(1000);
        long largestCombination = finder.getMaxSimilarPrimeNumbersCombinationLength();
        assertEquals(4, largestCombination);
    }

    @Test
    void shouldCompleteCalculationFor123mlnInLessThen10minutes() {
        Instant t1 = Instant.now();
        SimilarPrimeNumbersFinder finder = new SimilarPrimeNumbersFinder(123_456_789);
        int result = finder.getMaxSimilarPrimeNumbersCombinationLength();
        Instant t2 = Instant.now();
        long duration = Duration.between(t1, t2).toMinutes();
        assertTrue(duration<10);
        assertEquals(4333, result);
    }

}