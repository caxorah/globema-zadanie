package zadanie;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class SimilarNumbersProcessorTest {


    @Test
    void shouldReturnAllSimilarPrimesCombinationsFor1000() {
        int maxNumber = 1000;
        Set<Integer> expected = Set.of(149, 419, 491, 941);
        List<Integer> primeNumbers = PrimeNumbers.findPrimeNumbersNotGreaterThan(maxNumber);
        SimilarNumbersProcessor similarNumbersProcessor = new SimilarNumbersProcessor(primeNumbers);
        long maxLength = similarNumbersProcessor.getMaxOccurrences();
        assertEquals(expected.size(), maxLength);
    }


}