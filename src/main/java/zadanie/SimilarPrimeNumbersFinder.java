package zadanie;

import java.util.*;

public class SimilarPrimeNumbersFinder {

    private final List<Integer> allPrimeNumbers;
    private final SimilarNumbersProcessor similarNumbersProcessor;

    public SimilarPrimeNumbersFinder(int maxNumber) {
        allPrimeNumbers = PrimeNumbers.findPrimeNumbersNotGreaterThan(maxNumber);
        similarNumbersProcessor = new SimilarNumbersProcessor(allPrimeNumbers);
    }

    public int getMaxSimilarPrimeNumbersCombinationLength() {
        return similarNumbersProcessor.getMaxOccurrences();
    }


}
