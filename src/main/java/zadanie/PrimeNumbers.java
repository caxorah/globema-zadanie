package zadanie;

import org.apache.commons.math3.primes.Primes;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PrimeNumbers {

    public static List<Integer> findPrimeNumbersNotGreaterThan(int maxNumber) {
        if (maxNumber<=2) return new LinkedList<>();

        return IntStream.rangeClosed(2, maxNumber)
                .parallel()
                .boxed()
                .filter(Primes::isPrime)
                .collect(Collectors.toList());
    }

}
