package zadanie;

import java.util.*;

public class SimilarNumbersProcessor {

    private final HashMap<String, Integer> occurrences;

    public SimilarNumbersProcessor(List<Integer> numbers) {
        occurrences = mapOccurrences(numbers);
    }

    public Integer getMaxOccurrences() {
        return occurrences.values().stream().max(Integer::compareTo).orElse(0);
    }

    private HashMap<String,Integer> mapOccurrences(List<Integer> numbers) {
        HashMap<String, Integer> collect = new HashMap<>();
        numbers.stream()
                .map(this::numberToStringRepresentation)
                .forEach(s -> collect.put(s, (collect.containsKey(s) ? (collect.get(s)+1) : 1 )));
        return collect;
    }


    private String numberToStringRepresentation(int number) {
        char[] charsRepresentation = Integer.toString(number).toCharArray();
        Arrays.sort(charsRepresentation);
        return String.valueOf(charsRepresentation);
    }

}
