package zadanie;


public class Run {

    public static void main(String[] args) {

        int lookupForNumber = 123_456_789;
        SimilarPrimeNumbersFinder finder = new SimilarPrimeNumbersFinder(lookupForNumber);
        System.out.println(finder.getMaxSimilarPrimeNumbersCombinationLength());
    }

}
